import React, { Component } from 'react';
import '../styles/App.css';
import GameBox from './GameBox'

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      missed_letters: [],
      empty_guessed_letters: Array(11).join(".").split(".") // by default word to guess have max 11 letters
    }
    window.addEventListener('keydown', (event) => this.catchLetterFromKeyboardEvent(event))
  }

  componentWillMount(){
    this.getTheWord()
  }

  getTheWord(){ //get word from Api dictionary
    fetch("https://hangman-myapi.herokuapp.com/")
      .then(response=> {
        if (!response.ok) {
          throw Error(response.statusText)
        }
        return response.json()
      })
      .then(json => {
        this.setState({
          current_word: json.word.toLowerCase(),
          letters_to_guess: json.word.split("").length
        })
        this.createEncryptedWord(json.word.toLowerCase())
        this.cleanDataFromPreviousGame()
        }
      )
      .catch(error => {
      })
  }

  createEncryptedWord(word){
    const word_length = word.length
    this.setState({ first_letter_position: this.state.empty_guessed_letters.length - word_length + 1}) // first letter position from left site
    let guessed_letters = this.state.empty_guessed_letters
    word.split("").map((letter, i) => guessed_letters[this.state.first_letter_position + i] = "#")
    this.setState({guessed_letters: guessed_letters}) // sample for 10 letter word ["", "", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"]
  }

  catchLetterFromKeyboardEvent(event){
    if (event.keyCode < 91 && event.keyCode > 64 && !this.state.end_game){ // only A-Z
      this.decipherGuessedLetters(event.key)
    }
  }

  decipherGuessedLetters(letter){
    let missed = this.state.missed_letters
    let guessed = this.state.guessed_letters
    let word = this.state.current_word

    if (missed.indexOf(letter) === -1 && guessed.indexOf(letter) === -1){ // make actions for only not user previous letters 
      
      if (word.includes(letter)){ // player guessed letter
        const first_position = this.state.first_letter_position
        let current_guessed_letters = guessed
        for ( let i = 0 ; i < word.split("").length ; i++ ){ // fill guessed letter on all positions
          if (word[i] === letter){
            current_guessed_letters[first_position + i] = letter // replace # to letter
            this.setState({letters_to_guess: this.state.letters_to_guess - 1})
            this.setState({ guessed_letters: current_guessed_letters })
          }
          if (this.state.letters_to_guess === 0) // left 0 letters to guest - WIN
            this.setState({ end_game: "win" })
        }
      }

      else { //player not guessed letter
        this.setState({ missed_letters: missed.concat(letter)})
        if (missed.concat(letter).length === 12) 
          this.setState({ end_game: "lose"})
      }
    }
  }

  cleanDataFromPreviousGame(){
    this.setState({
      end_game: "",
      missed_letters: [],
      empty_guessed_letters: Array(11).join(".").split(".")
    })
  }

  render() {
    return (
      <div className="App">
        <GameBox 
          end_game={this.state.end_game}
          guessed_letters={this.state.guessed_letters} 
          current_word={this.state.current_word} 
          missed_letters={this.state.missed_letters}
          run_new_game={() => this.getTheWord()}/>
      </div>
    );
  }
}

export default App;
