import React, { Component } from 'react';
import '../styles/hangman_elements.css';
import Bar from '../img/bar.png'
import Head from '../img/head.png'
import Neck from '../img/neck.png'
import Corpus from '../img/corpus.png'
import RightArm from '../img/right-arm.png'
import LeftArm from '../img/left-arm.png'
import RightHand from '../img/left-hand.png'
import LeftHand from '../img/left-hand.png'
import RightLeg from '../img/right-leg.png'
import LeftLeg from '../img/left-leg.png'
import RightFoot from '../img/right-foot.png'
import LeftFoot from '../img/left-foot.png'

class HangmanElements extends Component {

	renderHangmanElement(class_name, element, width, height){
		return(
			<img alt="" className={class_name} src={element} width={width ? width : ""} height={height ? height : ""}/>
		)
	}

  render() {
  	let wrong_answers = this.props.missed_letters.length
    return (
      <div className="relative_hangman_box" >
        { wrong_answers > 0 ? this.renderHangmanElement("han_bar", Bar, "132", "40") : true}
        { wrong_answers > 1 ? this.renderHangmanElement("han_head", Head, "90") : true}
        { wrong_answers > 2 ? this.renderHangmanElement("han_neck", Neck, "20") : true}
        { wrong_answers > 3 ? this.renderHangmanElement("han_corpus", Corpus, "50") : true}
        { wrong_answers > 4 ? this.renderHangmanElement("han_right_arm", RightArm, "50") : true}
        { wrong_answers > 5 ? this.renderHangmanElement("han_left_arm", LeftArm, "50") : true}
        { wrong_answers > 6 ? this.renderHangmanElement("han_right_hand", RightHand, "13") : true}
        { wrong_answers > 7 ? this.renderHangmanElement("han_left_hand", LeftHand, "13") : true}
        { wrong_answers > 8 ? this.renderHangmanElement("han_right_leg", RightLeg, "38") : true}
        { wrong_answers > 9 ? this.renderHangmanElement("han_left_leg", LeftLeg, "38") : true}
        { wrong_answers > 10 ? this.renderHangmanElement("han_right_foot", RightFoot, "38") : true}
        { wrong_answers > 11 ? this.renderHangmanElement("han_left_foot", LeftFoot, "38") : true}
      </div>
    );
  }
}

export default HangmanElements;
