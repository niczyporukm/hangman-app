import React, { Component } from 'react';
import '../styles/game_box.css';
import LetterBox from './LetterBox'
import MissedLetter from './MissedLetter'
import HangmanElements from './HangmanElements'

class GameBox extends Component {

  renderGameOverView(){
    return(
      <div className="game_over">
        <div className="background">
        </div>
          <div className="game_over_text">
            {this.props.end_game === "lose" ? "GAME OVER" : "WIN"}
          </div>
          <div onClick={() => this.runNewGame()} className="new_game">
            NEW WORD
        </div> 
      </div>
    )
  }

  runNewGame(){
    this.props.run_new_game()
  }

  render() {
    let mis_letters = this.props.missed_letters
    return (
      <div className="game_box" >
        {this.props.end_game ? this.renderGameOverView() : "" }
        <div className="background_triangle_top"/>
        <div className="background_triangle_bottom"/>
        <div className="background_square"/>
        <div className="top_content_container">
          <div className="hangman_container">
            <HangmanElements missed_letters={mis_letters}/>
          </div>
          
          <div className="missing_section_container">
            <div className="you_missed">
              YOU MISSED:
            </div>
            <div className="missed_letters_container">
            {mis_letters.map((letter, i) => <MissedLetter key={i} letter={letter.toUpperCase()} />)}
            </div>
          </div>
        </div>
        <div className="button_boxes_container">
          {this.props.guessed_letters ? this.props.guessed_letters.map((gues_letter, i) => 
            <LetterBox key={i} letter={gues_letter} missed_letters={mis_letters} /> ) : ""}
        </div>
      </div>
    );
  }
}

export default GameBox;
