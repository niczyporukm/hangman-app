import React, { Component } from 'react';
import '../styles/missed_letter.css';

class MissedLetter extends Component {

  render() {
    return (
      <div className="missed_letter_box" >
        {this.props.letter} 
      </div>
    );
  }
}

export default MissedLetter;
