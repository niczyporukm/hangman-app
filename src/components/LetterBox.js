import React, { Component } from 'react';
import '../styles/letter_box.css';

class LetterBox extends Component {

  render() {
    return (
      <div className={`letter_box ${this.props.letter ? 'non_empty_box' : ''}`} >
      	{this.props.letter !== "#" ? this.props.letter.toUpperCase() : ""}
      </div>
    );
  }
}

export default LetterBox;
